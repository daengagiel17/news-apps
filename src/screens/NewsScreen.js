import React from 'react';
import axios from 'axios';
import {StyleSheet, View, Text, ScrollView, StatusBar} from 'react-native';
import NewsItem from '../components/NewsItem.component';

export default class NewsScreen extends React.Component {
  constructor(props) {
    super(props);

    this.state = {
      listNews: [],
    };
  }

  componentDidMount() {
    // call axios
    axios({
      method: 'GET',
      url:
        'http://newsapi.org/v2/everything?q=bitcoin&from=2020-09-21&sortBy=publishedAt&apiKey=f114280494694a4ba9b83366b382a315',
    })
      .then((res) => {
        console.info('Data :', res.data.articles[0].title);
        this.setState({
          listNews: res.data.articles,
        });
      })
      .catch((err) => {
        console.error(err);
      });
  }

  render() {
    return (
      <View>
        <StatusBar backgroundColor={'#3B50EB'} barStyle="light-content" />
        <View style={styles.header}>
          <Text style={styles.headerTitle}>NEWS FOR YOU</Text>
        </View>
        <ScrollView style={styles.body}>
          {this.state.listNews.map((news, index) => (
            <NewsItem
              key={index}
              image={news.urlToImage}
              url={news.url}
              title={news.title}
              name={news.author}
              date={news.publishedAt}
              description={news.description}
            />
          ))}
        </ScrollView>
      </View>
    );
  }
}

const styles = StyleSheet.create({
  header: {
    flexDirection: 'row',
    justifyContent: 'center',
    padding: 15,
    backgroundColor: '#3B50EB',
    borderBottomLeftRadius: 20,
    borderBottomRightRadius: 20,
  },
  headerTitle: {
    fontFamily: 'segoe-ui',
    fontWeight: 'bold',
    color: '#FFFFFF',
    fontSize: 20,
  },
  body: {
    padding: 5,
    backgroundColor: '#FFFFFF',
  },
});
